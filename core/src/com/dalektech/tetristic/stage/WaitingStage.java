/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.dalektech.tetristic.GameController;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.actor.gui.Countdown;
import com.dalektech.tetristic.actor.gui.PlayerList;
import com.dalektech.tetristic.actor.gui.StartGameButton;
import com.dalektech.tetristic.model.common.Game;
import com.dalektech.tetristic.utils.Assets;
import com.dalektech.tetristic.utils.S;

/**
 * Stage where players are waiting for the game to start.
 */
public class WaitingStage extends Stage {
    /**
     * GameController used in order to list players and start the countdown.
     */
    private GameController gameController;

    /**
     * True if the music is being played.
     */
    private boolean musicPlayed = false;

    /**
     * Music played when the host click on "Start Game".
     */
    private Music music;

    /**
     * Ctor for WaitingStage
     * @param gameController
     */
    public WaitingStage(GameController gameController) {
        this.gameController = gameController;
        initializeViewport();
        initializeActors();
        this.gameController = gameController;
        this.music = Gdx.audio.newMusic(Gdx.files.internal(Assets.INTRO_MUSIC));
    }

    /**
     * Initialize the viewport and the camera of the stage.
     */
    private void initializeViewport() {
        getViewport().setWorldSize(Tetristic.WIDTH, Tetristic.HEIGHT);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Tetristic.WIDTH, Tetristic.HEIGHT);
        getViewport().setCamera(camera);
    }

    /**
     * Initialize the actor of the stage.
     */
    private void initializeActors() {
        Image background = Tetristic.getAssets().createImageFromTexture(Assets.IMG_MENU_BACKGROUND);
        addActor(background);

        // Player List label
        Label listPlayer = new Label(S.LIST_PLAYERS, new Label.LabelStyle(
                Tetristic.getAssets().getFont(Assets.MAIN_FONT_16),
                Color.PINK)
        );
        listPlayer.setPosition((float) Tetristic.WIDTH * .15f, (float) Tetristic.HEIGHT * .85f);
        addActor(listPlayer);

        // List of players waiting the game to start.
        PlayerList players = new PlayerList(gameController);
        players.setPosition((float) Tetristic.WIDTH * .15f,
                (float) Tetristic.HEIGHT * .75f);
        addActor(players);

        // The start button for the host.
        StartGameButton start = new StartGameButton(gameController);
        addActor(start);

        // The countdown.
        Countdown countdown = new Countdown(gameController);
        addActor(countdown);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (gameController.getGame().getCountdown() != Game.DEFAULT_COUNTDOWN && !musicPlayed) {
            this.music.play();
            musicPlayed = true;
        }
    }

    @Override
    public void dispose() {
        super.dispose();

        music.dispose();
    }
}
