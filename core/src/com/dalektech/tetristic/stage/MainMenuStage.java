/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.dalektech.tetristic.GameController;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.actor.gui.DialogTextButton;
import com.dalektech.tetristic.actor.gui.TetristicTextButtonStyle;
import com.dalektech.tetristic.utils.Assets;
import com.dalektech.tetristic.utils.S;


/**
 * Main menu.
 * <p/>
 * Point of entry of the application
 */
public class MainMenuStage extends Stage {
    private DialogTextButton enterName;
    private DialogTextButton joinGame;
    private final GameController gameController;

    private boolean requestSent = false;

    public MainMenuStage(GameController gameController) {
        this.gameController = gameController;
        Gdx.input.setInputProcessor(this);
        initializeViewport();
        initializeActors();
    }

    private void initializeViewport() {
        getViewport().setWorldSize(Tetristic.WIDTH, Tetristic.HEIGHT);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Tetristic.WIDTH, Tetristic.HEIGHT);
        getViewport().setCamera(camera);
    }

    private void initializeActors() {
        Image background = Tetristic.getAssets().createImageFromTexture(Assets.IMG_MENU_BACKGROUND);

        // Create Button
        enterName = new DialogTextButton(S.CREATE, S.DIALOG_ENTER_NAME);
        enterName.getButton().setPosition((float) Tetristic.WIDTH * .17f, (float) Tetristic.HEIGHT * .45f);

        // Join Button
        joinGame = new DialogTextButton(S.JOIN, S.DIALOG_JOIN);
        joinGame.getButton().setPosition((float) Tetristic.WIDTH * .177f, (float) Tetristic.HEIGHT * .4f);

        // Exit Button
        TextButton exitButton = new TextButton(S.EXIT, new TetristicTextButtonStyle());
        exitButton.setPosition((float) Tetristic.WIDTH * .178f, (float) Tetristic.HEIGHT * .35f);
        exitButton.addListener(new ChangeListener() { // Adding a ChangeListener that will be triggered when there will be a click.
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.exit(0);
            }
        });

        // Title
        Label titleLabel = new Label(S.APP_NAME, new Label.LabelStyle(
                Tetristic.getAssets().getFont(Assets.MAIN_FONT_40),
                Color.WHITE));
        titleLabel.setPosition((float) Tetristic.WIDTH * .04f, (float) Tetristic.HEIGHT * .58f);

        // Adding actors
        addActor(background);
        addActor(exitButton);
        addActor(titleLabel);
        addActor(enterName);
        addActor(joinGame);
    }

    @Override
    public boolean keyTyped(char character) {
        if ((character >= 'a' && character <= 'z')
                || (character >= 'A' && character <= 'Z')
                || (character >= '0' && character <= '9')) {
            enterName.keyType(character);
            joinGame.keyType(character);
            return true;
        } else if ((int) character == 13 || (int) character == 10) {
            enterName.keyEnterPress();
            joinGame.keyEnterPress();
            return true;
        } else if ((int) character == 27) {
            enterName.keyEscapePress();
            joinGame.keyEscapePress();
            return true;
        } else if ((int) character == 8) {
            enterName.keyDeletePress();
            joinGame.keyDeletePress();
            return true;
        }

        return super.keyTyped(character);
    }

    @Override
    public void draw() {
        super.draw();

        // Putting dialog on top of the screen.
        if (enterName.isDialogVisible()) {
            enterName.setZIndex(1000);
        } else if (joinGame.isDialogVisible()) {
            joinGame.setZIndex(1000);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (!requestSent) {

            if (joinGame.isValidated()) {
                if (enterName.isValidated()) {
                    requestSent = true;

                    this.gameController.onJoinGame(
                            enterName.getDialog().getInputValue(),
                            joinGame.getDialog().getInputValue()
                    );
                } else if (!enterName.isDialogVisible()) {
                    Gdx.input.setOnscreenKeyboardVisible(true);
                    enterName.setVisible(true);
                }
            } else if (enterName.isValidated()) {
                requestSent = true;
                this.gameController.onCreateGame(
                        enterName.getDialog().getInputValue()
                );
            }
        }
    }
}