/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.dalektech.tetristic.GameController;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.actor.Board;
import com.dalektech.tetristic.actor.gui.PlayerList;
import com.dalektech.tetristic.actor.gui.WinOrLost;
import com.dalektech.tetristic.listeners.OnCompleteLineListener;
import com.dalektech.tetristic.model.common.User;
import com.dalektech.tetristic.utils.Assets;
import com.dalektech.tetristic.utils.GestureStage;

/**
 * Stage for the actual Tetris game.
 */
public class GameStage extends GestureStage implements OnCompleteLineListener, GestureDetector.GestureListener {
    /**
     * Step (in seconds) between each step down for the current bloc.
     */
    private static final float STEP = 0.5f;

    /**
     * Time since last step (falling down one row)
     */
    private float fallTimer;

    /**
     * Whether the game is lost or not
     */
    private boolean lost = false;

    /**
     * Whether the board has been drawn at least once or not.
     * <p/>
     * Is used to prevent action from happening before the user can see it (first loading)
     */
    private boolean firstDraw = false;

    /**
     * Whether the background music has been started or not
     */
    private boolean musicStarted = false;

    /**
     * Key locker: the player must release the pressed key in order to use one again
     */
    private boolean keyLocked = true;

    /**
     * The Board actor holding the bricks
     */
    private Board board;

    /**
     * Game controller holding the network operation callbacks
     */
    private final GameController gameController;

    private WinOrLost winOrLost;

    private Music music;

    /**
     * Ctor for GameStage
     *
     * @param gameController game controller for network operations
     */
    public GameStage(GameController gameController) {
        super();
        this.gameController = gameController;

        initializeViewport();
        initializeActors();
        this.fallTimer = 0;
    }

    /**
     * Initialize viewport and camera
     */
    private void initializeViewport() {
        getViewport().setWorldSize(Tetristic.WIDTH, Tetristic.HEIGHT);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Tetristic.WIDTH, Tetristic.HEIGHT);
        getViewport().setCamera(camera);
    }

    /**
     * Initialize actors (board and textures)
     */
    private void initializeActors() {
        Image background = Tetristic.getAssets().createImageFromTexture(Assets.IMG_GAME_BACKGROUND);
        addActor(background);

        this.board = new Board(this);
        this.board.setPosition(Tetristic.WIDTH * .08f, Tetristic.HEIGHT * .093f);
        addActor(this.board);

        PlayerList players = new PlayerList(gameController);
        players.setPosition((float) Tetristic.WIDTH * .73f,
                (float) Tetristic.HEIGHT * .50f);
        addActor(players);

        music = Gdx.audio.newMusic(Gdx.files.internal(Assets.BACKGROUND_MUSIC));

        winOrLost = new WinOrLost();
        addActor(winOrLost);
    }

    @Override
    public void draw() {
        super.draw();
        this.firstDraw = true;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        // if the user has lost the game, or the board hasn't been drawn at least once
        // don't do anything
        if (lost || !firstDraw) {
            return;
        }

        if (!musicStarted) {
            music.play();
            musicStarted = true;
        }

        this.board.setPermanentLines(this.gameController.getUser().getPermanentLines());
        this.makeBrickFall(delta);
        this.applyInput();

        this.board.act(delta);
        if (win()) {
            this.board.remove();
        }
    }

    /**
     * Make the brick fall one row down in the board if it can.
     * <p/>
     * Also check whether it is stuck or not, and spawn a new brick if necessary, or lose the
     * game if the brick has reached the top line.
     *
     * @param delta game-loop delta
     */
    private void makeBrickFall(float delta) {
        // if the step has been reached
        if (this.fallTimer > STEP) {
            // check whether the brick is stuck (can't go any farther down)
            if (this.board.isBrickStuck()) {
                // if it is, try and fix it and check whether the player loses the game or not
                if (this.board.fixBrickStructure()) {
                    this.lose();
                }
            }

            // move the brick one row down and reinit the timer
            this.board.moveBrickDown();
            this.fallTimer = 0;
        } else {
            this.fallTimer += delta;
        }
    }

    /**
     * Check player input and execute necessary actions
     */
    private void applyInput() {
        // DOWN: accelerate the fall of the brick
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            if (!keyLocked) {
                if (this.board.isBrickStuck()) {
                    if (this.board.fixBrickStructure()) {
                        lose();
                    }
                } else {
                    this.board.moveBrickDown();
                }
            }
        }

        // LEFT
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (!keyLocked) {
                this.board.moveLeft();
                keyLocked = true;
            }
        }

        // RIGHT
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            if (!keyLocked) {
                this.board.moveRight();
                keyLocked = true;
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            if (!keyLocked) {
                this.board.rotateBrickLeft();
                keyLocked = true;
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.E)) {
            if (!keyLocked) {
                this.board.rotateBrickRight();
                keyLocked = true;
            }
        }

        // Unlock keys if released
        if (!Gdx.input.isKeyPressed(Input.Keys.DOWN)
                && !Gdx.input.isKeyPressed(Input.Keys.LEFT)
                && !Gdx.input.isKeyPressed(Input.Keys.RIGHT)
                && !Gdx.input.isKeyPressed(Input.Keys.A)
                && !Gdx.input.isKeyPressed(Input.Keys.E)) {
            keyLocked = false;
        }
    }

    /**
     * Declare the loss of the player
     */
    private void lose() {
        this.gameController.onLoseGame();
        this.board.remove();
        lost = true;
        winOrLost.setIsWinner(false);
    }

    private boolean win() {
        boolean gameFinished = false;
        if (gameController.getGame() != null && gameController.getUser() != null &&
                !gameController.getUser().isLoser()) {
            gameFinished = true;

            if (!gameController.getGame().getHost().getName().equals(
                    gameController.getUser().getName()) &&
                    !gameController.getGame().getHost().isLoser()) {
                gameFinished = false;
            }

            for (User user : gameController.getGame().getUsers()) {
                if (!user.getName().equals(gameController.getUser().getName()) &&
                        !user.isLoser()) {
                    gameFinished = false;
                }
            }
        }

        if (gameFinished) {
            winOrLost.setIsWinner(true);
        }


        return gameFinished;
    }

    @Override
    public void onCompleteLine() {
        this.gameController.onCompleteLine();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        if (velocityX == 0 && velocityY  == 0) {
            keyLocked = false;
            return false;
        }

        if (Math.abs(velocityX) > Math.abs(velocityY)) {
            if (velocityX > 0f) {
                if (!keyLocked) {
                    this.board.moveRight();
                    keyLocked = true;
                }
            } else {
                if (!keyLocked) {
                    this.board.moveLeft();
                    keyLocked = true;
                }
            }
        } else {
            if (velocityY < 0) {
                if (!keyLocked) {
                    this.board.rotateBrickRight();
                    keyLocked = true;
                }
            } else {
                if (!keyLocked) {
                    if (this.board.isBrickStuck()) {
                        if (this.board.fixBrickStructure()) {
                            lose();
                        }
                    } else {
                        this.board.moveBrickDown();
                        if (this.board.isBrickStuck()) {
                            if (this.board.fixBrickStructure()) {
                                lose();
                            }
                        } else {
                            this.board.moveBrickDown();
                        }
                    }

                    keyLocked = true;
                }
            }
        }

        return true;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void dispose() {
        super.dispose();
        this.music.dispose();
    }
}
