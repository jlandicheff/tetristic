/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Asset references and handling
 */
public final class Assets {
    public final AssetManager assetManager = new AssetManager();
    public static final String IMG_MENU_BACKGROUND = "img/background.jpg";
    public static final String IMG_GAME_BACKGROUND = "img/game_background.jpg";
    public static final String IMG_DIALOG_BACKGROUND = "img/dialog_background.jpg";
    public static final String IMG_BLUR_BACKGROUND = "img/blur_background.png";
    public static final String MAIN_FONT_16 = "font/font_16.fnt";
    public static final String MAIN_FONT_40 = "font/font_40.fnt";
    public static final String MAIN_FONT_72 = "font/font_72.fnt";
    public static final String BACKGROUND_MUSIC = "sfx/music.mp3";
    public static final String INTRO_MUSIC = "sfx/intro.mp3";

    public Assets() {
    }

    public Image createImageFromTexture(String name) {
        return new Image(getTexture(name));
    }

    private Texture getTexture(String name) {
        return assetManager.get(name, Texture.class);
    }

    public BitmapFont getFont(String name) {
        return assetManager.get(name, BitmapFont.class);
    }

    public void loadMainMenuAssets() {
        assetManager.load(IMG_MENU_BACKGROUND, Texture.class);
        assetManager.load(IMG_DIALOG_BACKGROUND, Texture.class);
        assetManager.load(IMG_BLUR_BACKGROUND, Texture.class);
        assetManager.load(MAIN_FONT_16, BitmapFont.class);
        assetManager.load(MAIN_FONT_40, BitmapFont.class);
    }

    public void loadGameAssets() {
        assetManager.load(IMG_GAME_BACKGROUND, Texture.class);
        assetManager.load(MAIN_FONT_72, BitmapFont.class);
        assetManager.load(BACKGROUND_MUSIC, Music.class);
        assetManager.load(INTRO_MUSIC, Music.class);
    }

    public void loadWaitingForPlayerAssets() {
        assetManager.load(IMG_MENU_BACKGROUND, Texture.class);
        assetManager.load(MAIN_FONT_16, BitmapFont.class);
        assetManager.load(MAIN_FONT_72, BitmapFont.class);
    }
}
