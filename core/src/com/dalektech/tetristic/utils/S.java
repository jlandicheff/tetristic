/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.utils;

/**
 * UI text references
 */
public final class S {
    public static final String APP_NAME = "Tetristic";
    public static final String CREATE = "Create Game";
    public static final String JOIN = "Join Game";
    public static final String EXIT = "Exit Game";
    public static final String LIST_PLAYERS = "List PLAYERS";
    public static final String DIALOG_ENTER_NAME = "ENTER YOUR NAME";
    public static final String DIALOG_JOIN = "USER TO JOIN";
    public static final String START_GAME = "START GAME";
    public static final String GAME_WIN = "YOU WIN!";
    public static final String GAME_LOST = "YOU LOST!";
}
