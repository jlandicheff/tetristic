/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.input.GestureDetector;

/**
 * Screen with input multiplexer to handle Keyboard/mouse and touch input
 */
public class MultiInputScreen extends FixedFpsScreen {
    private InputMultiplexer inputMultiplexer;
    private GestureStage gestureStage;

    public MultiInputScreen(GestureStage stage, int fps) {
        super(stage, fps);
        this.gestureStage = stage;
        this.inputMultiplexer = new InputMultiplexer();
    }

    @Override
    public void show() {
        super.show();

        inputMultiplexer.addProcessor(gestureStage);
        inputMultiplexer.addProcessor(new GestureDetector(20, .5f, 2, .15f, gestureStage));
        Gdx.input.setInputProcessor(inputMultiplexer);
    }
}
