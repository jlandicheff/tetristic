/**
 * This file is part of WANTED: Bad-ou-Alyve.
 * <p/>
 * WANTED: Bad-ou-Alyve is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * WANTED: Bad-ou-Alyve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with WANTED: Bad-ou-Alyve.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Generic screen with a stage that fixes FPS at which the stage is rendered
 */
public class FixedFpsScreen extends ScreenAdapter {

    private final Stage stage;
    private final float fps;

    private float accumulator;

    public FixedFpsScreen(final Stage stage, final int fps) {
        this.stage = stage;
        this.fps = 1.0f / fps;
        accumulator = fps;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Max frame time
        if (delta > .2f)
            delta = .2f;

        accumulator += delta;

        // We update simulation as much as needed
        while (accumulator >= fps) {
            stage.act(); // Update

            accumulator -= fps;
        }

        // We still draw once
        stage.draw(); // Draw
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
