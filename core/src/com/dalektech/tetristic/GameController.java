/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic;

import com.dalektech.tetristic.listeners.OnCompleteLineListener;
import com.dalektech.tetristic.listeners.OnCreateGameListener;
import com.dalektech.tetristic.listeners.OnJoinGameListener;
import com.dalektech.tetristic.listeners.OnLoseGameListener;
import com.dalektech.tetristic.listeners.OnStartGameListener;
import com.dalektech.tetristic.listeners.callbacks.OnGameCreatedListener;
import com.dalektech.tetristic.listeners.callbacks.OnGameJoinedListener;
import com.dalektech.tetristic.listeners.callbacks.OnGameLostListener;
import com.dalektech.tetristic.listeners.callbacks.OnGameStartedListener;
import com.dalektech.tetristic.model.common.Game;
import com.dalektech.tetristic.model.common.User;
import com.dalektech.tetristic.utils.network.NetworkOperation;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Game controller dealing the network operations with the server
 */
public class GameController implements OnCreateGameListener, OnJoinGameListener, OnStartGameListener,
        OnLoseGameListener, OnCompleteLineListener {

    /**
     * Tetristic server URI
     */
    private static final String URL = "http://tetristic.dalektech.com";

    /**
     * Create a game URI
     * <p/>
     * Arg: host's username
     */
    private static final String URL_CREATE = URL + "/create/%s";

    /**
     * Join a game URI
     * <p/>
     * Args: host's username, player's username
     */
    private static final String URL_JOIN = URL + "/join/%s/%s";

    /**
     * Initiate the countdown to the start of the game URI
     * <p/>
     * Arg: host's username
     */
    private static final String URL_START = URL + "/start/%s";

    /**
     * Get the current state of a game URI
     * <p/>
     * Arg: host's username
     */
    private static final String URL_GAME_INFO = URL + "/game/%s";

    /**
     * Get the current state of a user URI
     * <p/>
     * Arg: username
     */
    private static final String URL_USER_INFO = URL + "/user/%s";

    /**
     * Declare the loss of a player URI
     * <p/>
     * Arg: player's username
     */
    private static final String URL_LOSE = URL + "/lose/%s";

    /**
     * Declare the completion of a line, to give to other players URI
     * <p/>
     * Args: host's username, player's username
     */
    private static final String URL_GIVE_LINE = URL + "/giveline/%s/%s";

    /**
     * Username of the current user.
     * <p/>
     * Used till the corresponding User object can be retrieved from the server
     */
    private String username;

    /**
     * Current user
     */
    private final User user;

    /**
     * Current state of the game, retrieved from the server
     */
    private Game game;

    /**
     * Callback when on game creation has been dealt with by the server
     */
    private final OnGameCreatedListener onGameCreatedListener;

    /**
     * Callback when joining a game has been taken into account by the server
     */
    private final OnGameJoinedListener onGameJoinedListener;

    /**
     * Callback when countdown initialization has been dealt with by the server
     */
    private final OnGameStartedListener onGameStartedListener;

    /**
     * Callback when the loss of a player has been taken into account by the server
     */
    private final OnGameLostListener onGameLostListener;

    /**
     * Ctor for GameController
     * <p/>
     * Initializes callbacks for network operations.
     *
     * @param onGameCreatedListener game creation callback
     * @param onGameJoinedListener  game joining callback
     * @param onGameStartedListener game started callback
     * @param onGameLostListener    game lost callback
     */
    public GameController(OnGameCreatedListener onGameCreatedListener, OnGameJoinedListener onGameJoinedListener,
                          OnGameStartedListener onGameStartedListener, OnGameLostListener onGameLostListener) {
        this.onGameCreatedListener = onGameCreatedListener;
        this.onGameJoinedListener = onGameJoinedListener;
        this.onGameStartedListener = onGameStartedListener;
        this.onGameLostListener = onGameLostListener;

        this.user = new User();
    }

    /*****************************************
     *
     *          ROUTINE NETWORK OPS
     *
     ******************************************/

    /**
     * Start repetitive pulling of information about the current user and the current game.
     */
    private void start() {
        Observable.interval(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .doOnNext(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        if (game != null)
                            updateGame();

                        updateUser();
                    }
                }).subscribe();
    }

    /**
     * Pull information about the current game from the server
     */
    private void updateGame() {
        new NetworkOperation<>(URL_GAME_INFO, Game.class, this.game.getHost().getName())
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnSuccess(new Action1<Game>() {
                    @Override
                    public void call(Game game) {
                        GameController.this.game.copyFrom(game);
                    }
                }).subscribe();
    }

    /**
     * Pull information about the current user from the server
     */
    private void updateUser() {
        new NetworkOperation<>(URL_USER_INFO, User.class, this.username)
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnSuccess(new Action1<User>() {
                    @Override
                    public void call(User user) {
                        GameController.this.user.copyFrom(user);
                    }
                }).subscribe();
    }

    /*****************************************
     * SPECIFIC NETWORK OPS
     ******************************************/

    @Override
    public void onCreateGame(String username) {
        System.out.println("Sending create game request");
        this.username = username;

        new NetworkOperation<>(URL_CREATE, Game.class, username)
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        System.out.println("Could not create game");
                    }
                })
                .doOnSuccess(new Action1<Game>() {
                    @Override
                    public void call(Game game) {
                        GameController.this.game = game;

                        onGameCreatedListener.onGameCreated();
                        start();
                    }
                }).subscribe();
    }

    @Override
    public void onJoinGame(final String username, final String hostUsername) {
        System.out.println("Sending join game request");
        this.username = username;

        new NetworkOperation<>(URL_JOIN, Game.class, username, hostUsername)
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        System.out.println("Could not join game");
                    }
                })
                .doOnSuccess(new Action1<Game>() {
                    @Override
                    public void call(Game game) {
                        if (game != null) {
                            GameController.this.game = game;

                            onGameJoinedListener.onGameJoined();
                            start();
                        } else {
                            System.out.println("Could not find the game");
                        }
                    }
                }).subscribe();
    }

    @Override
    public void onStartGame() {
        System.out.println("Launching countdown request");

        new NetworkOperation<>(URL_START, Game.class, username)
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        System.out.println("An error occured");
                    }
                })
                .doOnSuccess(new Action1<Game>() {
                    @Override
                    public void call(Game game) {
                        if (game == null) {
                            System.out.println("Could not find the game. Maybe you're not the host!");
                        } else {
                            onGameStartedListener.onGameStartedListener();
                        }
                    }
                }).subscribe();
    }

    @Override
    public void onLoseGame() {
        System.out.println("Declaring loss of the game to the server");

        new NetworkOperation<>(URL_LOSE, Game.class, username)
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        System.out.println("An error occurred");
                    }
                })
                .doOnSuccess(new Action1<Game>() {
                    @Override
                    public void call(Game game) {
                        if (game == null) {
                            System.out.println("Could not find game");
                        } else {
                            onGameLostListener.onGameLost();
                        }
                    }
                }).subscribe();
    }

    @Override
    public void onCompleteLine() {
        System.out.println("Sending line to other players");

        new NetworkOperation<>(URL_GIVE_LINE, Game.class, game.getHost().getName(), username)
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        System.out.println("An error occurred");
                    }
                })
                .doOnSuccess(new Action1<Game>() {
                    @Override
                    public void call(Game game) {
                        if (game == null) {
                            System.out.println("Could not find game");
                        } else {
                            System.out.println("Gave line to other players");
                        }
                    }
                }).subscribe();
    }

    /*****************************************
     *
     *              ACCESSORS
     *
     ******************************************/

    /**
     * Get the current user, updated with information from the server
     *
     * @return the current state of the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Get the current game, updated with information from the server
     *
     * @return the current state of the game
     */
    public Game getGame() {
        return game;
    }
}
