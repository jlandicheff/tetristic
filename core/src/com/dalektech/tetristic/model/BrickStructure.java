/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.model;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * The shape of the brick
 * <p/>
 * Represents the position of the blocks relative to each other
 */
public abstract class BrickStructure implements IBrickStructure {
    /**
     * Amount of different shapes
     */
    public static final int AMOUNT = 7;

    /**
     * The current rotation (on a 4 direction-basis) of the structure
     */
    protected int currentState;

    /**
     * The number of different directions the structure can face - 1
     */
    protected int maxState;

    /**
     * List of blocks in the shape represented by 2-axis coordinates
     */
    protected ArrayList<Vector2> structure;

    /**
     * Ctor for BrickStructure
     * <p/>
     * Initialize the shape of the structure
     */
    protected BrickStructure() {
        maxState = 1;
        currentState = 0;
        initializeStructure();
    }

    /**
     * Getter for structure
     *
     * @return the structure (shape) of the brick
     */
    public ArrayList<Vector2> getStructure() {
        return this.structure;
    }

    @Override
    public void rotateRight() {
        changeState(maxState + currentState + 1);
    }

    @Override
    public void rotateLeft() {
        changeState(maxState + currentState - 1);
    }

    protected void changeState(int state) {
        currentState = state % maxState;
        structure.clear();
    }
}
