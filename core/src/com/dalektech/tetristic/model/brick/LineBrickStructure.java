/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.model.brick;

import com.badlogic.gdx.math.Vector2;
import com.dalektech.tetristic.model.BrickStructure;

import java.util.ArrayList;

/**
 * Straight line-shaped brick structure
 */
public class LineBrickStructure extends BrickStructure {

    @Override
    public void initializeStructure() {
        maxState = 2;
        structure = new ArrayList<>();

        structure.add(new Vector2(0, 0));
        structure.add(new Vector2(0, -1));
        structure.add(new Vector2(0, 1));
        structure.add(new Vector2(0, 2));
    }

    @Override
    protected void changeState(int state) {
        super.changeState(state);

        switch (currentState) {
            case 0:
                structure.add(new Vector2(0, 0));
                structure.add(new Vector2(0, -1));
                structure.add(new Vector2(0, 1));
                structure.add(new Vector2(0, 2));
                break;
            case 1:
                structure.add(new Vector2(0, 0));
                structure.add(new Vector2(-1, 0));
                structure.add(new Vector2(1, 0));
                structure.add(new Vector2(2, 0));
                break;
            default:
                structure.add(new Vector2(0, 0));
                structure.add(new Vector2(0, -1));
                structure.add(new Vector2(0, 1));
                structure.add(new Vector2(0, 2));
                break;
        }
    }
}
