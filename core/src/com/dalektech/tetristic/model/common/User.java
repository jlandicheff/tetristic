/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.model.common;

/**
 * Player in a Tetristic game
 */
public class User {
    /**
     * Username of the player
     */
    private String name;

    /**
     * Current number of permanent lines given by other players
     */
    private int permanentLines;

    /**
     * Whether or not the player has been eliminated from the game they are currently playing
     */
    private boolean loser;

    /**
     * Set the attributes in order to copy another User instance
     *
     * @param user the user instance to copy
     */
    public void copyFrom(User user) {
        this.name = user.getName();
        this.permanentLines = user.getPermanentLines();
        this.loser = user.isLoser();
    }

    /**
     * Getter for name
     *
     * @return the username of the player
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for permanentLines
     *
     * @return the current number of permanent lines given by other players
     */
    public int getPermanentLines() {
        return permanentLines;
    }

    /**
     * Getter for loser
     *
     * @return whether or not the player has been eliminated from the game they are currently playing
     */
    public boolean isLoser() {
        return loser;
    }
}
