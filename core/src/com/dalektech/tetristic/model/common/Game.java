/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.model.common;

import java.util.List;

/**
 * State of a multiplayer game
 */
public class Game {
    /**
     * Default countdown value to the beginning of the game (when not initiated by the host)
     */
    public static final int DEFAULT_COUNTDOWN = 999;

    /**
     * User who created the game
     * <p/>
     * The name of the host is used by other players to join the game.
     */
    private User host;

    /**
     * Players who joined the game
     * <p/>
     * The host User is excluded from this list.
     */
    private List<User> users;

    /**
     * Current countdown to the beginning of the game
     *
     * @see Game#DEFAULT_COUNTDOWN
     */
    private int countdown;

    /**
     * Set the attributes in order to copy another Game instance
     *
     * @param game the game instance to copy
     */
    public void copyFrom(Game game) {
        this.host = game.getHost();
        this.users = game.getUsers();
        this.countdown = game.getCountdown();
    }

    /**
     * Getter for host
     *
     * @return the host of the game
     */
    public User getHost() {
        return host;
    }

    /**
     * Getter for users
     *
     * @return the users of the game
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Getter for countdown
     *
     * @return the current countdown to the beginning of the game
     */
    public int getCountdown() {
        return countdown;
    }
}
