/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.dalektech.tetristic.listeners.callbacks.OnGameCreatedListener;
import com.dalektech.tetristic.listeners.callbacks.OnGameJoinedListener;
import com.dalektech.tetristic.listeners.callbacks.OnGameLostListener;
import com.dalektech.tetristic.listeners.callbacks.OnGameStartedListener;
import com.dalektech.tetristic.stage.GameStage;
import com.dalektech.tetristic.stage.MainMenuStage;
import com.dalektech.tetristic.stage.WaitingStage;
import com.dalektech.tetristic.utils.Assets;
import com.dalektech.tetristic.utils.FixedFpsScreen;
import com.dalektech.tetristic.utils.MultiInputScreen;

public class Tetristic extends Game implements OnGameCreatedListener, OnGameJoinedListener,
        OnGameStartedListener, OnGameLostListener {

    public static final int WIDTH = 960;
    public static final int HEIGHT = 600;

    private Assets assets;
    private GameController gameController;

    private FixedFpsScreen mainMenuScreen;
    private MultiInputScreen gameScreen;
    private FixedFpsScreen waitingScreen;

    private MainMenuStage mainMenuStage;
    private WaitingStage waitingStage;
    private GameStage gameStage;

    public static Assets getAssets() {
        return ((Tetristic) Gdx.app.getApplicationListener()).assets;
    }

    public static Tetristic getGame() {
        return (Tetristic) Gdx.app.getApplicationListener();
    }

    @Override
    public void create() {
        assets = new Assets();
        Texture.setAssetManager(assets.assetManager);

        gameController = new GameController(this, this, this, this);
        initializeScreens();
    }

    private void initializeScreens() {
        assets.loadMainMenuAssets();
        assets.assetManager.finishLoading();
        this.mainMenuStage = new MainMenuStage(gameController);
        mainMenuScreen = new FixedFpsScreen(mainMenuStage, 30);

        assets.loadGameAssets();
        assets.assetManager.finishLoading();
        this.gameStage = new GameStage(gameController);
        gameScreen = new MultiInputScreen(gameStage, 40);

        assets.loadWaitingForPlayerAssets();
        assets.assetManager.finishLoading();
        this.waitingStage = new WaitingStage(gameController);
        waitingScreen = new FixedFpsScreen(waitingStage, 30);
    }

    @Override
    public void render() {
        if (gameController.getGame() != null) {
            if (gameController.getGame().getCountdown() <= 0) {
                this.displayGameScreen();
            } else {
                this.displayWaitingForPlayerScreen();
            }
        } else {
            this.displayMainMenuScreen();
        }

        super.render();
    }

    private void displayMainMenuScreen() {
        assets.loadMainMenuAssets();
        assets.assetManager.finishLoading();

        setScreen(this.mainMenuScreen);
    }

    private void displayGameScreen() {
        assets.loadGameAssets();
        assets.assetManager.finishLoading();

        setScreen(this.gameScreen);
    }

    private void displayWaitingForPlayerScreen() {
        assets.loadWaitingForPlayerAssets();
        assets.assetManager.finishLoading();

        setScreen(this.waitingScreen);
    }

    @Override
    public void dispose() {
        super.dispose();
        assets.assetManager.dispose();
    }

    @Override
    public void onGameCreated() {
        System.out.println("New game: " + this.gameController.getGame().getHost().getName());
    }

    @Override
    public void onGameJoined() {
        System.out.println("Joined game: " + this.gameController.getGame().getHost().getName());
    }

    @Override
    public void onGameStartedListener() {
        System.out.println("Launched countdown for the current game... Be ready!");
    }

    @Override
    public void onGameLost() {
        System.out.println("Game lost signal received by the server");
    }
}
