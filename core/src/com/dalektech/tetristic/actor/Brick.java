/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.dalektech.tetristic.model.BrickStructure;
import com.dalektech.tetristic.model.brick.LBrickStructure;
import com.dalektech.tetristic.model.brick.LReverseBrickStructure;
import com.dalektech.tetristic.model.brick.LineBrickStructure;
import com.dalektech.tetristic.model.brick.OBrickStructure;
import com.dalektech.tetristic.model.brick.TBrickStructure;
import com.dalektech.tetristic.model.brick.ZBrickStructure;
import com.dalektech.tetristic.model.brick.ZReverseBrickStructure;

import java.util.Random;

/**
 * Brick falling down till a collision is detected underneath it.
 * <p/>
 * Bricks have different shapes, depending on the structure tying their blocks together.
 */
class Brick extends Actor {
    /**
     * The size of a block
     */
    public static final int SIZE = 32;

    /**
     * The structure (shape) of the brick
     */
    private BrickStructure brickStructure;

    /**
     * Ctor for Brick
     * <p/>
     * Initializes a brick with random shape and color, and centers it at the top of the game area.
     */
    public Brick() {
        this.initialize();
    }

    /*****************************************
     *
     *              INITIALIZATION
     *
     ******************************************/

    /**
     * Initialize a brick with random shape and color
     * <p/>
     * Center the brick at the top of the board.
     */
    private void initialize() {
        this.brickStructure = pickRandomBrickStructure();
        this.setColor(pickRandomColor());
    }

    /**
     * Get a random brick shape
     *
     * @return a random brick structure
     */
    private BrickStructure pickRandomBrickStructure() {
        Random rand = new Random();

        switch (rand.nextInt(BrickStructure.AMOUNT)) {
            case 0:
                return new LBrickStructure();
            case 1:
                return new LReverseBrickStructure();
            case 2:
                return new ZBrickStructure();
            case 3:
                return new ZReverseBrickStructure();
            case 4:
                return new OBrickStructure();
            case 5:
                return new LineBrickStructure();
            case 6:
                return new TBrickStructure();
            default:
                return new OBrickStructure();
        }
    }

    /**
     * Get a random color amongst an 8-color palette
     *
     * @return a random brick color
     */
    private Color pickRandomColor() {
        Random rand = new Random();

        switch (rand.nextInt(7)) {
            case 0:
                return Color.RED;
            case 1:
                return Color.BLUE;
            case 2:
                return Color.ORANGE;
            case 3:
                return Color.PURPLE;
            case 4:
                return Color.YELLOW;
            case 5:
                return Color.GREEN;
            case 6:
                return Color.FIREBRICK;
            default:
                return Color.CORAL;
        }
    }

    /*****************************************
     *
     *              ACCESSORS
     *
     ******************************************/

    /**
     * Getter for brick structure
     *
     * @return the structure of the brick
     */
    public BrickStructure getBrickStructure() {
        return brickStructure;
    }

    /*****************************************
     * LIBGDX
     ******************************************/

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.end();

        ShapeRenderer renderer = new ShapeRenderer();
        renderer.setProjectionMatrix(batch.getProjectionMatrix());

        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.translate(getParent().getX(), getParent().getY(), 0);
        renderer.setColor(this.getColor());

        // Draw each of the blocs with the brick color
        for (Vector2 block : this.brickStructure.getStructure()) {
            if (getY() + block.y * SIZE < Board.HEIGHT * SIZE) {
                renderer.rect(
                        getX() + block.x * SIZE + 2, getY() + block.y * SIZE + 2, SIZE - 2, SIZE - 2
                );
            }
        }

        renderer.end();
        batch.begin();
    }
}
