/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.dalektech.tetristic.GameController;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.model.common.User;
import com.dalektech.tetristic.utils.Assets;

import java.util.List;

/**
 * Display the list of player of a game.
 */
public class PlayerList extends Group {
    /**
     * The list of player.
     */
    private List<User> players;

    /**
     * GameController to access the model.
     */
    private final GameController gameController;

    /**
     * Font that will draw the message.
     */
    private final BitmapFont font;

    /**
     * Name of the host.
     */
    private String hostName;

    /**
     * Ctor for PlayerList.
     * @param gameController
     */
    public PlayerList(GameController gameController) {
        this.gameController = gameController;
        setSize(Tetristic.WIDTH * .18f, Tetristic.HEIGHT * .07f);
        font = Tetristic.getAssets().getFont(Assets.MAIN_FONT_16);
    }

    /**
     * LibGDX.
     */

    @Override
    public void act(float delta) {
        super.act(delta);
        if (hostName == null) {
            hostName = gameController.getGame().getHost().getName();
        }
        players = gameController.getGame().getUsers();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        GlyphLayout layout = new GlyphLayout();
        layout.setText(font, hostName);
        font.draw(batch, hostName, getX() + getWidth() / 2 - layout.width / 2, getTop());

        for (int i = 0; i < players.size(); i++) {
            if (!players.get(i).isLoser()) {
                layout = new GlyphLayout();
                layout.setText(font, players.get(i).getName());
                font.draw(batch, players.get(i).getName(),
                        getX() + getWidth() / 2 - layout.width / 2,
                        getTop() - ((i + 1) * 35));
            }
        }
    }
}