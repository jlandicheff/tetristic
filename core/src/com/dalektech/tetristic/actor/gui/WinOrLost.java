/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.dalektech.tetristic.GameController;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.model.common.User;
import com.dalektech.tetristic.utils.Assets;
import com.dalektech.tetristic.utils.S;

import java.util.List;

/**
 * Actor that will print "You Win" or "You Lost".
 */
public class WinOrLost extends Group {
    private final BitmapFont font;

    /**
     * Used in order to change the state of displaying.
     */
    private float counter;

    /**
     * Used in order to blink the font.
     */
    private boolean displaying;

    /**
     * True if the game is finished. Else false.
     */
    private boolean gameFinished;

    /**
     * True if the user is the winner. Else false.
     */
    private boolean isWinner;

    /**
     * Ctor of WinOrLost
     */
    public WinOrLost() {
        font = Tetristic.getAssets().getFont(Assets.MAIN_FONT_72);
        displaying = false;
        counter = .0f;
        gameFinished = false;
        isWinner = false;
    }

    /**
     * Setter of the isWinner variable.
     * @param isWinner
     */
    public void setIsWinner(boolean isWinner) {
        this.isWinner = isWinner;
        gameFinished = true;
    }


    /**
     * LibGDX.
     */

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        if (gameFinished && displaying) {
            String text = "";
            if (isWinner) {
                text = S.GAME_WIN;
            } else {
                text = S.GAME_LOST;
            }

            // Creating a layout to get the width of the text to display
            GlyphLayout layout = new GlyphLayout();
            layout.setText(font, text);

            font.draw(batch, text,
                    Tetristic.WIDTH * .5f - layout.width / 2,
                    Tetristic.HEIGHT * .5f - layout.height / 2);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        // Used to blink the message
        counter += delta;
        if (counter >= 0.44f) {
            counter = 0;
            displaying = !displaying;
        }
    }
}
