/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.dalektech.tetristic.GameController;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.utils.S;

/**
 * StarGame button displayed only for the host.
 */
public class StartGameButton extends Group {
    /**
     * Boolean that will determine if the button may be draw.
     */
    private boolean ready;

    /**
     * GameController to access the model.
     */
    private final GameController gameController;

    /**
     * Ctor for GameController.
     * @param gameController
     */
    public StartGameButton(final GameController gameController) {
        TextButton button = new TextButton(S.START_GAME, new TetristicTextButtonStyle());
        button.setPosition(Tetristic.WIDTH * .15f, Tetristic.HEIGHT * .30f);
        this.gameController = gameController;

        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                gameController.onStartGame();
            }
        });

        addActor(button);
    }

    /**
     * LibGDX
     */

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (ready) {
            super.draw(batch, parentAlpha);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (!gameController.getGame().getHost().getName().equals(gameController.getUser().getName())) {
            return;
        }

        if (!ready) {
            if (gameController.getGame().getUsers().size() > 0) {
                ready = true;
            }
        }
    }
}
