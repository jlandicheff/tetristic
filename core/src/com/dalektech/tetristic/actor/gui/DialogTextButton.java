/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.dalektech.tetristic.utils.S;

/**
 * This actor is a text button that will trigger a custom dialog when clicked.
 */
public class DialogTextButton extends Group {
    /**
     * The custom dialog.
     */
    private final Dialog dialog;

    /**
     * The TextButton.
     */
    private final TextButton button;

    /**
     * Ctor for DialogTextButton.
     * @param text
     * @param message
     */
    public DialogTextButton(String text, String message) {
        dialog = new Dialog();
        dialog.setMessage(message);

        button = new TextButton(text, new TetristicTextButtonStyle());
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.input.setOnscreenKeyboardVisible(true);
                dialog.setVisible(true);
            }
        });

        addActor(button);
        addActor(dialog);
    }

    /**
     * To use when a character is typed.
     * @param character
     */
    public void keyType(char character) {
        if (dialog.isVisible()) {
            dialog.keyTyped(character);
        }
    }

    /**
     * To use when the enter key is pressed.
     */
    public void keyEnterPress() {
        if (dialog.isVisible()) {
            dialog.keyEnterPress();
        }
    }

    /**
     * To use when the escape key is pressed.
     */
    public void keyEscapePress() {
        if (dialog.isVisible()) {
            dialog.keyEscapePress();
        }
    }

    /**
     * To use when the delete key is pressed.
     */
    public void keyDeletePress() {
        if (dialog.isVisible()) {
            dialog.keyDeletePress();
        }
    }

    /**
     * Return true if the dialog is currently displayed.
     * @return
     */
    public boolean isDialogVisible() {
        return dialog.isVisible();
    }

    /**
     * Return true if the input value has been validated.
     * @return
     */
    public boolean isValidated() {
        return dialog.isValidated();
    }

    /**
     * Setter for the visibility of the dialog.
     * @param visible
     */
    public void setVisible(boolean visible) {
        dialog.setVisible(visible);
    }

    /**
     * Getter of the Dialog.
     * @return
     */
    public Dialog getDialog() {
        return dialog;
    }

    /**
     * Getter of the TextButton.
     * @return
     */
    public TextButton getButton() {
        return button;
    }


    /**
     * LibGDX.
     */

    @Override
    public void act(float delta) {
        super.act(delta);

        if (button.getText().equals(S.JOIN) &&
                dialog.isValidated()) {
            Dialog enterNameDialog = new Dialog();
            enterNameDialog.setMessage(S.DIALOG_ENTER_NAME);
            addActor(enterNameDialog);
            if (enterNameDialog.isValidated()) {
                System.out.println("start game now!");
            }
        }
    }
}