/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.dalektech.tetristic.GameController;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.model.common.Game;
import com.dalektech.tetristic.utils.Assets;

/**
 * Actor that print a countdown when a game is about to begin.
 */
public class Countdown extends Group {
    /**
     * GameController used in order to access models.
     */
    private final GameController gameController;

    /**
     * The font used in order to draw the countdown.
     */
    private final BitmapFont font;

    /**
     * Ctor for Countdown
     * @param gameController
     */
    public Countdown(GameController gameController) {
        this.gameController = gameController;
        font = Tetristic.getAssets().getFont(Assets.MAIN_FONT_72);
    }

    /**
     * LibGDX.
     */

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        // Getting the countdown
        int cd = gameController.getGame().getCountdown();

        // If the countdown has started
        if (cd != Game.DEFAULT_COUNTDOWN) {
            Integer countdown = gameController.getGame().getCountdown();
            GlyphLayout layout = new GlyphLayout();
            layout.setText(font, countdown.toString());
            font.draw(
                    batch,
                    countdown.toString(),
                    Tetristic.WIDTH * .5f - layout.width / 2,
                    Tetristic.HEIGHT * .5f + layout.height / 2);
        }
    }
}