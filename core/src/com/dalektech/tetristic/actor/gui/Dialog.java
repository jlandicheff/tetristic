/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.utils.Assets;

/**
 * Custom dialog.
 */
public class Dialog extends Group {
    /**
     * The message displayed on top of the input value.
     */
    private String message;

    /**
     * The value entered by the user.
     */
    private String inputValue;

    /**
     * The font that will draw message and input value.
     */
    private final BitmapFont font;

    /**
     * Background of the dialog.
     */
    private final Image dialogBackground;

    /**
     * Width of the message.
     */
    private float messageWidth;

    /**
     * Width of the inputValue.
     */
    private float inputValueWidth;

    /**
     * True if the dialog is currently displayed.
     */
    private boolean isVisible;

    /**
     * True if the inputValue has been validated.
     */
    private boolean isValidated;

    /**
     * Ctor for Dialog.
     */
    public Dialog() {
        isVisible = false;
        isValidated = false;

        font = Tetristic.getAssets().getFont(Assets.MAIN_FONT_16);

        Image bluredBackground = Tetristic.getAssets().createImageFromTexture(Assets.IMG_BLUR_BACKGROUND);
        setSize(bluredBackground.getImageWidth(), bluredBackground.getImageHeight());
        addActor(bluredBackground);

        dialogBackground = Tetristic.getAssets().createImageFromTexture(Assets.IMG_DIALOG_BACKGROUND);
        dialogBackground.setPosition(getDialogX(), getDialogY());
        addActor(dialogBackground);

        message = "";
        inputValue = "";

        inputValueWidth = 0f;
        messageWidth = 0f;
    }

    /**
     * Setter for message.
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
        GlyphLayout layout = new GlyphLayout();
        layout.setText(font, message);
        messageWidth = layout.width;
    }

    /**
     * Setter for the Input Value.
     * @param inputValue
     */
    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
        GlyphLayout layout = new GlyphLayout();
        layout.setText(font, inputValue);
        inputValueWidth = layout.width;
    }

    /**
     * Getter for inputValue.
     * @return
     */
    public String getInputValue() {
        return inputValue;
    }

    /**
     * Used when a char is pressed.
     * @param c
     */
    public void keyTyped(char c) {
        if (inputValue.length() == 20) {
            return;
        }

        inputValue += c;
        setInputValue(inputValue);
    }

    /**
     * Used when the enter key is pressed.
     */
    public void keyEnterPress() {
        Gdx.input.setOnscreenKeyboardVisible(false);
        if (inputValue.length() == 0) {
            return;
        }
        isVisible = false;
        isValidated = true;
    }

    /**
     * Used when the escape key is pressed.
     */
    public void keyEscapePress() {
        Gdx.input.setOnscreenKeyboardVisible(false);
        isVisible = false;
    }

    /**
     * Used when the delete key is pressed.
     */
    public void keyDeletePress() {
        if (inputValue.length() == 0) {
            return;
        }
        setInputValue(inputValue.substring(0, inputValue.length() - 1));
    }

    /**
     * Return the X position of the dialog.
     * @return
     */
    private float getDialogX() {
        return Tetristic.WIDTH / 2 - (dialogBackground.getWidth() / 2);
    }

    /**
     * Return the Y position of the dialog.
     * @return
     */
    private float getDialogY() {
        return Tetristic.HEIGHT * 0.65f;
    }

    /**
     * Setter of the visibility.
     * @param visible
     */
    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    /**
     * Getter of isValidated.
     * @return
     */
    public boolean isValidated() {
        return isValidated;
    }

    /**
     * Getter of isVisible.
     * @return
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * LibGDX.
     */

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (isVisible) {
            super.draw(batch, parentAlpha);

            font.draw(batch, message, getDialogX() + dialogBackground.getWidth() / 2 -
                    messageWidth / 2, getDialogY() + dialogBackground.getHeight() - 40);
            font.draw(batch, inputValue, getDialogX() + dialogBackground.getWidth() / 2 -
                    inputValueWidth / 2, getDialogY() + dialogBackground.getHeight() - 105);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }
}
