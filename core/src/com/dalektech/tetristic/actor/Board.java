/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.dalektech.tetristic.Tetristic;
import com.dalektech.tetristic.listeners.OnCompleteLineListener;
import com.dalektech.tetristic.model.Direction;

import java.util.HashMap;

/**
 * The game board
 * <p/>
 * Grid of cells holding the blocks that fell down, and the current brick falling.
 */
public class Board extends Group {

    /**
     * Height of the board (amount of cells)
     *
     * @see Brick#SIZE
     */
    public static final int HEIGHT = 15;

    /**
     * Width of the board (amount of cells)
     *
     * @see Brick#SIZE
     */
    private static final int WIDTH = 10;

    /**
     * Default color for empty cells
     */
    private static final Color EMPTY_CELL = Color.BLACK;

    /**
     * Color of permanent blocks given by other players
     */
    private static final Color PERMANENT_CELL = Color.SCARLET;

    /**
     * Position where the brick to come indicator should be rendered
     */
    private static final Vector2 NEXT_BRICK_POSITION = new Vector2(
            .66f * Tetristic.WIDTH - 3 * Brick.SIZE,
            .599f * Tetristic.HEIGHT - 3 * Brick.SIZE
    );

    /**
     * The brick currently in play (the one falling down)
     */
    private Brick currentBrick;

    /**
     * The next brick (for printing)
     */
    private Brick nextBrick;

    /**
     * The column of the origin of the current brick
     */
    private int brickCol;

    /**
     * The row of the origin of the current brick
     */
    private int brickRow;

    /**
     * Matrix representing the grid of the board
     * <p/>
     * A cell holds a color (either representing a block, or the default EMPTY_CELL color).
     */
    private HashMap<Vector2, Color> cells;

    /**
     * Matrix holding the Vector2 keys for the "cells" HashMap
     */
    private Vector2[][] cellKeys;

    /**
     * Number of permanent block lines given by other players
     */
    private int permanentLines;

    /**
     * Callback for completing a non-permanent line
     */
    private final OnCompleteLineListener onCompleteLineListener;

    /**
     * Ctor for Board
     */
    public Board(OnCompleteLineListener onCompleteLineListener) {
        super();
        this.onCompleteLineListener = onCompleteLineListener;

        initializeCells();

        this.nextBrick = new Brick();
        initializeBrick();
    }

    /**
     * Initialize the board with empty cells
     */
    private void initializeCells() {
        this.setPermanentLines(0);

        this.cells = new HashMap<>();
        this.cellKeys = new Vector2[10][15];

        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                Vector2 v = new Vector2(i, j);
                this.cellKeys[i][j] = v;
                this.cells.put(v, EMPTY_CELL);
            }
        }
    }

    /**
     * Set the current brick and its position.
     * <p/>
     * Also prepare the next brick.
     */
    private void initializeBrick() {
        this.removeActor(this.currentBrick);
        this.currentBrick = this.nextBrick;
        this.addActor(this.currentBrick);

        this.brickCol = WIDTH / 2 - 1;
        this.brickRow = HEIGHT + 1;

        this.updateBrickPosition();

        this.nextBrick = new Brick();
    }

    /**
     * Get the content (Color) of a cell at given coordinates
     *
     * @param col x-coordinate in the board
     * @param row y-coordinate in the board
     * @return the content of the cell
     */
    private Color getCellContent(int col, int row) {
        if (col >= WIDTH || col < 0 || row < 0 || row >= HEIGHT) {
            return EMPTY_CELL;
        }

        return this.cells.get(this.cellKeys[col][row]);
    }

    /*****************************************
     *
     *              BEHAVIOR
     *
     ******************************************/

    /**
     * Stick the current brick to the board
     *
     * @return whether the fixing operation made the player lose the game or not
     */
    public boolean fixBrickStructure() {
        boolean lost = false;

        // fix the blocks on the board
        for (Vector2 block : this.currentBrick.getBrickStructure().getStructure()) {
            // if a block is above the top line, the player has lost the game
            if (brickRow + block.y >= HEIGHT) {
                lost = true;
            }

            // check that the block is contained within the board's matrix
            if (brickCol + block.x < WIDTH && brickCol + block.x >= 0
                    && brickRow + block.y >= 0 && brickRow + block.y < HEIGHT) {
                this.cells.put(
                        this.cellKeys[brickCol + (int) block.x][brickRow + (int) block.y],
                        new Color(this.currentBrick.getColor())
                );
            }
        }

        // Spawn a new brick
        initializeBrick();

        // delete full lines if needed
        deleteFullLine();

        return lost;
    }

    /**
     * Move one column to the left if possible
     */
    public void moveLeft() {
        if (!checkBrickLateralCollision(Direction.Left)) {
            this.brickCol -= 1;
        }
    }

    /**
     * Move one column to the right if possible
     */
    public void moveRight() {
        if (!checkBrickLateralCollision(Direction.Right)) {
            this.brickCol += 1;
        }
    }

    /**
     * Move one row down
     */
    public void moveBrickDown() {
        this.brickRow -= 1;
    }

    public void rotateBrickLeft() {
        this.currentBrick.getBrickStructure().rotateLeft();

        if (this.isBrickStuck() && !this.checkBrickLateralCollision(Direction.Left)) {
            this.moveLeft();
            if (this.isBrickStuck()) {
                this.moveRight();
                this.currentBrick.getBrickStructure().rotateRight();
            }
        } else if (this.isBrickStuck() && !this.checkBrickLateralCollision(Direction.Right)) {
            this.moveRight();
            if (this.isBrickStuck()) {
                this.moveLeft();
                this.currentBrick.getBrickStructure().rotateRight();
            }
        } else if (this.checkBrickLateralCollision(Direction.Right) && !this.checkBrickLateralCollision(Direction.Left)) {
            this.moveLeft();
            if (this.isBrickStuck()) {
                this.moveRight();
                this.currentBrick.getBrickStructure().rotateRight();
            }
        } else if (this.checkBrickLateralCollision(Direction.Left) && !this.checkBrickLateralCollision(Direction.Right)) {
            this.moveRight();
            if (this.isBrickStuck()) {
                this.moveLeft();
                this.currentBrick.getBrickStructure().rotateRight();
            }
        }
    }

    public void rotateBrickRight() {
        this.currentBrick.getBrickStructure().rotateRight();

        if (this.isBrickStuck() && !this.checkBrickLateralCollision(Direction.Left)) {
            this.moveLeft();
            if (this.isBrickStuck()) {
                this.moveRight();
                this.currentBrick.getBrickStructure().rotateLeft();
            }
        } else if (this.isBrickStuck() && !this.checkBrickLateralCollision(Direction.Right)) {
            this.moveRight();
            if (this.isBrickStuck()) {
                this.moveLeft();
                this.currentBrick.getBrickStructure().rotateLeft();
            }
        } else if (this.checkBrickLateralCollision(Direction.Right) && !this.checkBrickLateralCollision(Direction.Left)) {
            this.moveLeft();
            if (this.isBrickStuck()) {
                this.moveRight();
                this.currentBrick.getBrickStructure().rotateLeft();
            }
        } else if (this.checkBrickLateralCollision(Direction.Left) && !this.checkBrickLateralCollision(Direction.Right)) {
            this.moveRight();
            if (this.isBrickStuck()) {
                this.moveLeft();
                this.currentBrick.getBrickStructure().rotateLeft();
            }
        }
    }

    /**
     * Remove every complete non-permanent line and have other blocks above fall down
     */
    private void deleteFullLine() {
        for (int i = this.permanentLines; i < HEIGHT; i++) {
            // Going through each line.
            boolean isLineFull = true;
            for (int j = 0; j < WIDTH; j++) {
                // Going through each column of the line.
                if (this.cells.get(this.cellKeys[j][i]).equals(EMPTY_CELL)) {
                    // Empty cell found, so skipping this line.
                    isLineFull = false;
                    break;
                }
            }

            if (isLineFull) {
                // initiate network operation
                this.onCompleteLineListener.onCompleteLine();

                for (int k = i; k < HEIGHT - 1; k++) {
                    // Going through each line that are on top of the full line.
                    for (int j = 0; j < WIDTH; j++) {
                        // Going through each column of that line, and replace the cell by the cell+1.
                        this.cells.put(this.cellKeys[j][k], this.cells.get(this.cellKeys[j][k + 1]));
                    }
                }
                i--;
            }
        }
    }

    /**
     * Set the number of permanent lines
     * <p/>
     * Insert the given number into the cells of the board.
     *
     * @param permanentLines the new number of permanent lines
     */
    public void setPermanentLines(int permanentLines) {
        this.permanentLines = permanentLines;

        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < permanentLines; j++) {
                this.cells.put(this.cellKeys[i][j], PERMANENT_CELL);
            }
        }
    }

    /*****************************************
     *
     *              LOGICS
     *
     ******************************************/

    /**
     * Check whether the brick is out of of bound.
     * <p/>
     * A brick is out of bound if one of its blocks is out of the board (except top border)
     *
     * @return whether or not the brick is out of bound.
     */
    private boolean checkBrickLateralCollision(final Direction dir) {
        int offset = dir == Direction.Left ? -1 : 1;

        for (Vector2 block : this.currentBrick.getBrickStructure().getStructure()) {
            if (brickCol + block.x + offset >= WIDTH
                    || brickCol + block.x + offset < 0) {
                return true;
            }
        }

        for (Vector2 block : this.currentBrick.getBrickStructure().getStructure()) {
            int targetCol = brickCol + (int) block.x + offset;
            int targetRow = brickRow + (int) block.y;

            if (!this.getCellContent(targetCol, targetRow).equals(EMPTY_CELL)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check whether the current brick is stuck or not.
     * <p/>
     * The brick is stuck if at least one of its blocks has another block underneath, or has reached
     * the bottom line of the board
     *
     * @return whether the current brick is stuck or not
     */
    public boolean isBrickStuck() {
        for (Vector2 block : this.currentBrick.getBrickStructure().getStructure()) {
            int targetCol = brickCol + (int) block.x;
            int targetRow = brickRow + (int) block.y - 1;

            if (targetRow < 0) {
                return true;
            }

            if (!getCellContent(targetCol, targetRow).equals(EMPTY_CELL)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set the coordinates (relative to the parent) of the current brick, from its coordinates
     * in the board's matrix
     */
    private void updateBrickPosition() {
        this.currentBrick.setX(this.brickCol * Brick.SIZE);
        this.currentBrick.setY(this.brickRow * Brick.SIZE);
    }

    /*****************************************
     * LIBGDX
     ******************************************/

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        ShapeRenderer renderer = new ShapeRenderer();
        renderer.setProjectionMatrix(batch.getProjectionMatrix());
        renderer.begin(ShapeRenderer.ShapeType.Filled);

        for (Vector2 block : this.cells.keySet()) {
            // Render all the cells as empty
            renderer.setColor(EMPTY_CELL);
            renderer.rect(
                    getX() + block.x * Brick.SIZE,
                    getY() + block.y * Brick.SIZE,
                    Brick.SIZE,
                    Brick.SIZE
            );

            // Draw every fixed block with its color
            if (!this.cells.get(block).equals(EMPTY_CELL)) {
                renderer.setColor(this.cells.get(block));
                renderer.rect(
                        getX() + block.x * Brick.SIZE + 2,
                        getY() + block.y * Brick.SIZE + 2,
                        Brick.SIZE - 2,
                        Brick.SIZE - 2
                );
            }
        }

        // Draw the next brick hint
        for (Vector2 block : this.nextBrick.getBrickStructure().getStructure()) {
            renderer.setColor(this.nextBrick.getColor());
            renderer.rect(
                    NEXT_BRICK_POSITION.x + block.x * Brick.SIZE + 2,
                    NEXT_BRICK_POSITION.y + block.y * Brick.SIZE + 2,
                    Brick.SIZE - 2,
                    Brick.SIZE - 2
            );
        }

        renderer.end();
        batch.begin();

        super.draw(batch, parentAlpha);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        this.updateBrickPosition();
    }
}
