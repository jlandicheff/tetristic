/*
    This file is part of Tetristic.

    Tetristic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tetristic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tetristic.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dalektech.tetristic.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.dalektech.tetristic.Tetristic;

public class DesktopLauncher {
    public static void main(String[] arg) {
        if (arg.length > 0 && arg[0].equals("licence")) {
            System.out.println("GNU GENERAL PUBLIC LICENSE");
            System.out.println("Version 3, 29 June 2007");
            System.out.println();
            System.out.println("Tetristic  Copyright (C) 2016  Hamajo");

            return;
        }

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.height = Tetristic.HEIGHT;
        config.width = Tetristic.WIDTH;
        config.resizable = false;
        new LwjglApplication(new Tetristic(), config);
    }
}
