# Tetristic #

Tetristic is a multiplayer Tetris game made with Libgdx.

# Server #

You may used your own Tetristic server by running the sources [available here](https://bitbucket.org/hamajo/server). 
You will have to modify the client's sources though in order to point it to the right URI.

### Platforms ###

The current platforms are currently supported:

* Linux
* OSX
* Windows
* Android

### Contributors ###

* JACQUEMAIN Maxime
* LANDICHEFF Jonathan
* RAHMOUN Hajar

### Licence ###

See *licence* file